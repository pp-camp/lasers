# -*- coding: utf-8 -*-
"""
Example for using Helios DAC libraries in python (using C library with ctypes)

NB: If you haven't set up udev rules you need to use sudo to run the program for it to detect the DAC.
"""

import ctypes

# Define point structure


class HeliosPoint(ctypes.Structure):
    #_pack_=1
    _fields_ = [('x', ctypes.c_uint16),
                ('y', ctypes.c_uint16),
                ('r', ctypes.c_uint8),
                ('g', ctypes.c_uint8),
                ('b', ctypes.c_uint8),
                ('i', ctypes.c_uint8)]


# Load and initialize library
HeliosLib = ctypes.cdll.LoadLibrary("./libHeliosDacAPI.so")
numDevices = HeliosLib.OpenDevices()
print("Found ", numDevices, "Helios DACs")

# Create sample frames
num_frames = 360
num_pts = 360
frames = [0 for x in range(num_frames)]
frameType = HeliosPoint * num_pts  # 1000
x = 0
y = 0
import math


def t(c):
    c /= 2
    c += 0.5
    return int(c * 0xfff)


#def tamagochi():


v = 255
import random
for i in range(num_frames):
    y = round(i * 0xFFF / 30)
    frames[i] = frameType()

    random.seed(i//10)
    for p in range(num_pts):

        # x = random.random()
        # y = random.random()
        # frames[i][p*2] = HeliosPoint(t(x), t(y), 0, 0, 0, 0)  # int(255*math.cos(p)),int(255*math.sin(p)),255,255)
        # # frames[i][p*2+1] = HeliosPoint(t(x), t(y), int(255 * math.cos(math.radians(p))), int(255 * math.sin(math.radians(p))), 255, 255)  # int(255*math.cos(p)),int(255*math.sin(p)),255,255)
        # frames[i][p*2+1] = HeliosPoint(t(x), t(y), 255, 255, 255, 255)
        # frames[i][p*2+2] = HeliosPoint(t(x), t(y), 0, 0, 0, 0)  # int(255*math.cos(p)),int(255*math.sin(p)),255,255)


        #        frames[i][p] = HeliosPoint(t(math.sin(math.radians((p+i)*2))), t(math.cos(math.radians((p+i)*2))),int(255*math.cos(p)),int(255*math.sin(p)),255,255)
        # x = math.sin(math.radians(360 / num_pts * p))
        # y = math.cos(math.radians(360 / num_pts * p))
        x = 0.7*math.sin(math.radians(360 / num_pts * p))
        y = 0.7*math.cos(math.radians(360 / num_pts * p)) + 0.3 * math.cos(math.cos(math.radians(i*4)))
        frames[i][p] = HeliosPoint(t(x), t(y), int(255 * math.cos(math.radians(p))), int(255 * math.sin(math.radians(p))), 255, 255)  # int(255*math.cos(p)),int(255*math.sin(p)),255,255)
#        frames[i][p] = HeliosPoint(t(math.sin(math.radians(360/num_pts*p))), t(math.cos(math.radians(360/num_pts*p))),255*math.cos(math.radians(i)),v,v,255)
    # frames[i][0] = HeliosPoint(t(0), t(0),255,255,255,255)
    # frames[i][1] = HeliosPoint(t(math.sin(i)), t(math.cos(i)),255,255,255,255)
#    frames[i][0] = HeliosPoint(t(math.sin(math.radians(i-1))), t(math.cos(math.radians(i-1))),255,255,255,255)
#    frames[i][1] = HeliosPoint(t(math.sin(math.radians(i))), t(math.cos(math.radians(i))),255,255,255,255)
    # frames[i][0] = HeliosPoint(0, 0,255,255,255,255)
    # frames[i][1] = HeliosPoint(0, 0xfff,255,255,255,255)
    # frames[i][2] = HeliosPoint(0xfff,0xfff,255,255,255,255)
    # frames[i][3] = HeliosPoint(0xff, 0,255,255,255,255)

    # for j in range(1000):
    #     if (j < 500):
    #         x = round(j * 0xFFF / 500)
    #     else:
    #         x = round(0xFFF - ((j - 500) * 0xFFF / 500))

    #     frames[i][j] = HeliosPoint(int(x),int(y),255,255,255,255)

# Play frames on DAC
for i in range(num_frames * 100):
    for j in range(numDevices):
        statusAttempts = 0
        # Make 512 attempts for DAC status to be ready. After that, just give up and try to write the frame anyway
        while (statusAttempts < 512 and HeliosLib.GetStatus(j) != 1):
            statusAttempts += 1
        HeliosLib.WriteFrame(j, 20000, 0, ctypes.pointer(frames[i % num_frames]), num_pts)  # Send the frame


HeliosLib.CloseDevices()
